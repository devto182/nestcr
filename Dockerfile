FROM node:18-alpine as base
WORKDIR /usr/app/src
COPY package*.json ./
EXPOSE 3000

FROM base as production
ENV NODE_ENV=production
RUN npm install
COPY . .
RUN npm run build
CMD ["npm", "run", "start"]
