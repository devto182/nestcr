# (devto) Nestjs Container Registry

API project sample using Nestjs with default configuration. Adding dockerization file to make a container service and ready to deploy.

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
npm install
```

## Running the app

```bash
# development
npm run start

# watch mode
npm run start:dev

# production mode
npm run start:prod
```

## Running Docker command

```bash
# build image
docker build -t <image-name> .

# run the image
docker run --rm -p -d 3000:3000 <image-name>
```

## Test

```bash
# unit tests
npm run test

# e2e tests
npm run test:e2e

# test coverage
npm run test:cov
```

## License

Nest is [MIT licensed](LICENSE).
